angular
    .module('starter.directives', [])
    .directive('beerItem', function() {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            controller: ['$scope', '$element', function($scope, $element) {
                this.$scope = $scope;
                this.$scope.brewery = $scope.beer._source.brewery
                this.$element = $element;
            }],
            templateUrl: 'templates/beer-item.html'
        };
    });