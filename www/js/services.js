angular.module('starter.services', ['elasticsearch'])
   .service('clientsearch', function (esFactory) {
       return esFactory({
           host: {
               host: 'bofur-us-east-1.searchly.com',
               port: 80,
               protocol: 'http',
               auth: 'paas:51c1e390d28105f28778806d246d7fec'
           },
           log: 'trace'
       });
   })
   .factory('BeerService', function ($http, clientsearch) {

       var serverUrl = "http://beerbelly.herokuapp.com/";

       function findById(id) {
          console.log(serverUrl+'beers/'+id);
          return $http.get(serverUrl+'beers/'+id);
       }
       function findByStyle(style) {
           return search("style:\"" + style + "\"&rows=10")
       }
       function findByBrewery(breweryid) {
           return search("breweryid:" + breweryid + "&rows=10")
       }

       function findByName(searchKey) {
           return search(searchKey);
       }

       function news() {
           return search("punk");
       }

       function tracker(ean) {
           var resul = $http.get(serverUrl+'tracker/app/trackerEan?ean='+ean);
           return resul;
       }

       function updateEan(ean, idBeer){
           return $http.get(serverUrl+'tracker/app/updateEan?ean='+ean+'&idBeer='+idBeer);
       }

       function search(search) {
           console.log('search:'+search);
          return clientsearch.search({
               index: 'beers',
               type: 'beer',
               body: {
                  query: {
                     match:{
                        name: search
                     }
                  }
               }
           });
       }

       function getBrewery(id) {console.log(serverUrl+"breweries/"+id);
           var result = $http.get(serverUrl+"breweries/"+id);
           console.log(result);
           return result;
       }


       return {
           get: findById,
           query: findByName,
           tracker: tracker,
           getBrewery: getBrewery,
           getByBrewery: findByBrewery,
           news: news,
           findByStyle: findByStyle,
           updateEan: updateEan
       }
   })
   .factory('FacebookService', function FacebookService($rootScope) {

       function handleStatusChange(session) {
           if (session.authResponse)
               authenticate();
       }

       function authenticate() {
           FB.api(
              '/me',
              {
                  fields: 'id, name, about, birthday, email, picture, cover, location'
              },
              function(response) {
                  if (!response.error) {

                      $rootScope.$apply(function() {
                          $rootScope.user = response;

                          FB.api('/'+response.id, {fields: 'cover'}, function(response) {
                              console.log(JSON.stringify(response));
                              $rootScope.user.cover = response.cover;
                          })
                      });

                  } else {
                      logout();
                  }
              }
           );
       }

       function logout() {
           FB.logout(function() {
               $rootScope.user = undefined;
           });
       }

       return {
           init: function() {
               FB.init({
                   appId: gAppID,
                   nativeInterface: CDV.FB,
                   useCachedDialogs: false
               });

               FB.getLoginStatus(handleStatusChange, false);
               FB.Event.subscribe('auth.statusChange', handleStatusChange);
//            updateAuthElements();
           },
           login: function() {
               FB.login(handleStatusChange, {scope: 'public_profile, email, user_friends'});
           },
           logout: logout,
           uninstall: function() {
               uninstallApp();
           }
       }

   });