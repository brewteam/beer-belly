angular.module('starter.services.style', [])
    .factory('StyleService', function ($http) {

        var serverUrl = "http://teca.bitnamiapp.com:8080/beerstore/";

        function getStyle(search) {
            var url = serverUrl+"styles/app/load?id="+search;
            var result = $http.get(url);
            return result;
        }

        function findAll(ornanization) {
            var url = serverUrl+"styles/app/findAll";
            var result = $http.get(url);
            return result;
        }

//        function getStyles(search) {
//            var url = serverUrl+"styles/app/findAll";
//            return $http.get(url);
//        }
//
        function getSubCategory(search) {
            var url = serverUrl+"subcategorys/app/load?id="+search;
            return $http.get(url);
        }

        return {
            get: getStyle,
            findAll:findAll,
            getSubCategory: getSubCategory
        }
    });
