angular.module('starter.controllers', ['starter.directives'])

    .controller('MainCtrl', function($scope, $ionicSideMenuDelegate, BeerService, $ionicSlideBoxDelegate) {

        $scope.logo = logoTitle;

        window.localStorage.setItem('brewery',null);
        window.localStorage.setItem('subcategory',null);
        window.localStorage.setItem('searchKey',"");

        $scope.leftButtons = [
            {
                type: 'button-icon button-clear ion-navicon',
                tap: function(e) {
                    $ionicSideMenuDelegate.toggleLeft($scope.$$childHead);
                }
            }

        ];

        BeerService.news().then(function(response) {
            console.log(response.hits.hits);
                $scope.newBeers = response.hits.hits;
                $ionicSlideBoxDelegate.update();
            });
    })

    .controller('DefaultCtrl', function($scope) {
        $scope.logo = logoTitle;
    })

    .controller('BeerIndexCtrl', function ($scope, $rootScope, $location, BeerService) {
        $scope.logo = logoTitle;
        $scope.ean = '';
        var beers = window.localStorage.getItem('beers');

        if (beers != null) {
            $scope.beers = JSON.parse(beers);
            $scope.searchKey = window.localStorage.getItem('searchKey');
        }

        $scope.search = function () {
            $scope.ean = '';
            $scope.beers = null;
            if ($scope.searchKey.length > 2) {
                $rootScope.blockLoading = true;
                BeerService.query($scope.searchKey)
                    .then(function(response) {
                        $scope.beers = response.hits.hits;
                        window.localStorage.setItem(
                            'beers',
                            JSON.stringify($scope.beers)
                        );
                        window.localStorage.setItem('searchKey', $scope.searchKey);
                        $rootScope.blockLoading = null;
                    });
            }
        };

        $scope.clearSearch = function() {
            $scope.searchKey = '';
            $scope.ean = '';
            $scope.beer = null;
            $scope.beers = null;
            $scope.brewery = null;
            window.localStorage.removeItem('beer');
            window.localStorage.removeItem('beers');
            window.localStorage.removeItem('brewery');
            window.localStorage.removeItem('searchKey');
            window.localStorage.removeItem('ean');
            $scope.subcategory = null;
            window.localStorage.removeItem('subcategory');
        };

        $scope.updateEan = function(ean, idBeer){
            console.log('updateEan:'+ean + ' idBeer:'+idBeer);
            BeerService.updateEan(ean, idBeer)
                .success(function(data, status, headers, config){
                    console.log('Success UpdateEan:');
                    console.log(data);
                    $scope.beers = data;
                    console.log($scope.beers.id)
                    $location.path( "/menu/beers/"+$scope.beers.id );
                });

        };

        $scope.scan = function() {
            cordova.exec(
                function(success){
                    var ean = success[0];
                    $scope.beers = null;
                    BeerService.query(ean)
                        .success(function(data, status, headers, config) {
                            $scope.beers = data.hits.hits;
/*                            if($scope.beers.length == 0){
                                BeerService.tracker(ean)
                                .success(function(data, status, headers, config){
                                        $scope.beers = data;
                                       if($scope.beers.length == 1){
                                           $location.path( "/menu/beers/"+$scope.beers[0].id );
                                       }else{
                                            window.localStorage.setItem(
                                                'beers',
                                                JSON.stringify($scope.beers)
                                            );
                                           $scope.searchKey = ean;
                                           $scope.ean = ean;
                                           window.localStorage.setItem('searchKey', $scope.searchKey);
                                           window.localStorage.setItem('ean',ean);
                                       }

                                });
                            }else{*/
                                if($scope.beers.length == 1){
                                    $location.path( "/menu/beers/"+$scope.beers[0]._id );
                                }else{
                                    console.error('Ooops! Mais de uma cerveja cadastrada com o mesmo EAN! Pode isso Arnaldo?');
                                    alert('Ooops! Mais de uma cerveja cadastrada com o mesmo EAN! Pode isso Arnaldo?')
                                }
                            //}
                    });

                },
                function(error){
                    console.error("Scan Failed: " + error);
                    alert("Failed: " + error);
                },
                "ScanditSDK",
                "scan",
                ["sWkappvQEeOBCWcjCcl8iBOHXVIuQYQO/HFkOZW397w", {"beep": true, "1DScanning" : true, "2DScanning" : true}]
            );

        };
    })
    .controller('BeerDetailCtrl', function ($scope, $stateParams, BeerService) {
        console.log($stateParams.id);
        var sBeer = window.localStorage.getItem('beer');
        console.log(sBeer)
        if(!angular.isString(sBeer) || sBeer == 'undefined'){
            console.log('beer is null');
//            verifica se a cerveja celecionada esta na lista da pesquisa
            var sBeers = window.localStorage.getItem('beers');
            if(angular.isString(sBeers) && sBeers != 'undefined'){
                console.log('beers existe');
                var beers = JSON.parse(sBeers);
                angular.forEach(beers, function(value,index){
                    console.log(value.id);
                    if(value.id==$stateParams.id){
                        $scope.beer = value;

                    }
                });
            }
        }else{
            console.log('else');
            var beer = JSON.parse(sBeer);
//            verifica se a cerveja que esta no localstorage e a mesma selecionada
            if(beer.id == $stateParams.id){
                console.log('beer id:'+beer.id);
                $scope.beer = beer;
            }
        }

        if( $scope.beer == null){

//          busca a cerveja selecionada
            BeerService.get($stateParams.id)
               .success(function(data) {
                  console.log(data);
                    $scope.beer = data;
                }).error(function(data) {
                  console.log('error');
                  console.log(data);
               });
                window.localStorage.setItem(
                    'beer',
                    JSON.stringify($scope.beer)
                );
        }else{
            window.localStorage.setItem(
                'beer',
                JSON.stringify($scope.beer)
            );
        }
    })

    .controller('BreweryDetailCtrl', function ($scope, $stateParams, BeerService) {
        BeerService.getBrewery($stateParams.id)
            .success(function(data, status, headers, config) {
                console.log(data);
                $scope.brewery = data;
            });
    })


    .controller('BeersByBreweryCtrl', function ($scope, $stateParams, BeerService) {
        BeerService.getByBrewery($stateParams.id)
            .success(function(data, status, headers, config) {
                $scope.beers = data.response.docs;
            });
    })

    .controller('TabCtrl', function($scope) {
        $scope.onControllerChanged = function(oldController, oldIndex, newController, newIndex) {
            console.log('Controller changed', oldController, oldIndex, newController, newIndex);
            console.log(arguments);
        };
    });

