var logoTitle = "Cervejoteca";

var module = angular.module('starter', ['ionic', 'starter.services', 'starter.controllers', 'starter.services.style', 'starter.controllers.style', 'elasticsearch'])
   .constant('$ionicLoadingConfig', {
      template: '<i class="ion-loading-c"></i>',
      showBackdrop: false
   })
   .factory('httpInterceptor', function ($injector, $rootScope) {
      return {
         'request': function (config) {
            var $ionicLoading = $injector.get("$ionicLoading");

            if (!$rootScope.blockLoading)
               $rootScope.loading = $ionicLoading.show();

            return config || $q.when(config);
         },

         'response': function (response) {
            $rootScope.loading && $rootScope.loading.hide();
            return response || $q.when(response);
         }
      };
   })
   .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

      $httpProvider.interceptors.push('httpInterceptor');

      $stateProvider

         .state('menu', {
            url: "/menu",
            abstract: true,
            templateUrl: "templates/side-menu.html",
            controller: 'MainCtrl'
         })

         .state('menu.home', {
            url: '/home',
            views: {
               'menuContent': {
                  templateUrl: 'templates/beer-index.html',
                  controller: 'BeerIndexCtrl'
               }
            }
         })

         .state('menu.detail', {
            url: '/beers/:id',
            views: {
               'menuContent': {
                  templateUrl: 'templates/beer-detail.html',
                  controller: 'BeerDetailCtrl'
               }
            }
         })

         .state('menu.breweryDetail', {
            url: '/brewery/:id',
            views: {
               'menuContent': {
                  templateUrl: 'templates/brewery-detail.html',
                  controller: 'BreweryDetailCtrl'
               }
            }
         })

         .state('menu.styleDetail', {
            url: '/style/:id',
            views: {
               'menuContent': {
                  templateUrl: 'templates/style-detail.html',
                  controller: 'StyleDetailCtrl'
               }
            }
         })

         .state('menu.style', {
            url: '/styles',
            views: {
               'menuContent': {
                  templateUrl: 'templates/style-index.html',
                  controller: 'StyleIndexCtrl'
               }
            }
         })
         .state('menu.categoryDetail', {
            url: '/category/:id',
            views: {
               'menuContent': {
                  templateUrl: 'templates/category-detail.html',
                  controller: 'CategoryDetailCtrl'
               }
            }
         })
         .state('menu.about', {
            url: '/about',
            views: {
               'menuContent': {
                  templateUrl: 'templates/about.html',
                  controller: 'DefaultCtrl'
               }
            }
         });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/menu/home');
   }])
   .run(['$ionicPlatform', function ($ionicPlatform) {
      $ionicPlatform.ready(function () {
         // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
         // for form inputs)
         if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
         }
         if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
         }
      });
   }]);
