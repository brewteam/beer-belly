angular.module('starter.controllers.style', [])
   .controller('StyleIndexCtrl', function ($scope, $stateParams, StyleService, BeerService) {
      var bcjp = 'BJCP - Beer Judge Certification Program';

      $scope.ornganizations = [
         {
            name: bcjp,
            descricao: ' The purpose of the Beer Judge Certification Program is to:\r\n'
               + 'Encourage knowledge, understanding, and appreciation of the world\'s diverse beer, mead, and cider styles;\r\n'
               + 'Promote, recognize, and advance beer, mead, and cider tasting, evaluation, and communication skills; and \r\n'
               + 'Develop standardized tools, methods, and processes for the structured evaluation, ranking and feedback of beer, mead, and cider.'},
         {name: 'Beer Advocate'},
         {name: 'Rate Beer'}
      ];

      function findStyles(organization) {
         //Temporario ate implementacao dos estilos dos demais organization
         if (organization.name !== bcjp) {
            organization.styles = [];
         } else {

            StyleService.findAll(organization)
               .success(function (data) {
                  organization.styles = data;
               });
         }
      }

      $scope.stylesVisible = function (organization) {
         return organization.styles && organization.styles.length > 0;
      };

      $scope.toggleStyles = function (organization) {
         if (!organization.styles)
            findStyles(organization);

         organization.showingStyles = !organization.showingStyles;
      };

      $scope.toggleGroup = function (group) {
         if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
         } else {
            $scope.shownGroup = group;
         }
      };

      $scope.isGroupShown = function (group) {
         return $scope.shownGroup === group;
      };
   })
   .controller('StyleDetailCtrl', function ($scope, $stateParams, StyleService) {
      StyleService.get($stateParams.id)
         .success(function (data, status, headers, config) {
            $scope.style = data;
         });

   })
   .controller('CategoryDetailCtrl', function ($scope, $stateParams, StyleService) {
      console.log($stateParams.id)
      StyleService.getSubCategory($stateParams.id)
         .success(function (data, status, headers, config) {
            $scope.subcategory = data;
         });
   })
   .controller('BeersByStyle', function ($scope, $stateParams, BeerService) {
      BeerService.findByStyle($stateParams.id)
         .success(function (data, status, headers, config) {
            $scope.beers = data.response.docs;
         });

   });
